#!/bin/bash

export PATH=/bin:/usr/bin:/usr/local/bin
TODAY=`date +"%d%b%Y"`
BACKUP_RETAIN_DAYS=100   ## Number of days to keep local backup copy
DB_BACKUP_PATH='/home/venkat/deftouch/backup'
DATABASE_NAME='mydb'

#################################################################

echo "Backup started for database - ${DATABASE_NAME}"
mkdir -p ${DB_BACKUP_PATH}/${TODAY}
sftp -i ~/.ssh/id_rsa venkat@35.200.226.203 <<< 'get '"${DB_BACKUP_PATH}"'/'"${TODAY}"'/mydb-'"${TODAY}"'.db '"${DB_BACKUP_PATH}"'/'"${TODAY}"''



###shell id=1qtv0ktUPnvTpIVT47fLKs5VTdIVakq4X###

gdrive upload -p 1qtv0ktUPnvTpIVT47fLKs5VTdIVakq4X ${DB_BACKUP_PATH}/${TODAY}/mydb-${TODAY}.db

#gdrive upload  /home/venkat/deftouch/backup/01Apr2019/mydb-01Apr2019.db
##### Remove backups older than {BACKUP_RETAIN_DAYS} days  #####

DBDELDATE=`date +"%d%b%Y" --date="${BACKUP_RETAIN_DAYS} days ago"`

if [ ! -z ${DB_BACKUP_PATH} ]; then
      cd ${DB_BACKUP_PATH}
      if [ ! -z ${DBDELDATE} ] && [ -d ${DBDELDATE} ]; then
            rm -rf ${DBDELDATE}
      fi
fi

### End of script ####
