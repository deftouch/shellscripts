#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

SERVICES=( 'mysqld' )

for i in "${SERVICES[@]}"
do
	`pgrep $i >/dev/null 2>&1`
STATS=$(echo $?)
	if [[  $STATS == 1  ]]
	then
	service $i start
	`pgrep $i >/dev/null 2>&1`
RESTART=$(echo $?)
	if [[  $RESTART == 0  ]]
	then
	if [ -f "/tmp/$i" ];
	then
	rm /tmp/$i
	fi
	echo "Mysql Was restarted On $(hostname) at $(date) becoz it was stopped" | mail -s "$i Restart Successfull In Development" venkat@deftouch.co,naveen@deftouch.co,subrata@deftouch.co,keshav@deftouch.co
	else
	if [ ! -f "/tmp/$i" ]; then
	touch /tmp/$i
	echo "Mysql is not running, even not able to start On $(hostname) at $(date) Please take proper action immediately" | mail -s "$i Restart Successfull In Development" venkat@deftouch.co,naveen@deftouch.co,subrata@deftouch.co,keshav@deftouch.co
	fi
	fi
	fi
	done
	exit 0;

