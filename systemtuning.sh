#!/bin/bash
red='\033[1;31m'
green='\033[1;32m'
yellow='\033[1;33m'
NC='\033[0m'

set -ex
echo "${green} Increasing the file descriptors${NC}"

sudo sysctl -w fs.file-max=100000

echo "${green} Increasng the Tcp port Range${NC}"

sudo sysctl -w net.ipv4.ip_local_port_range="1024 65535"

echo "${green} Decreasing the Fin_timeout${NC}"

sudo sysctl -w net.ipv4.tcp_fin_timeout=30

echo "${green} SYN Cookies${NC}"

sudo sysctl -w net.ipv4.tcp_syncookies=1

echo "${green} Incresng the Request backlog${NC}"

sudo sysctl -w net.core.somaxconn=1024

echo "${green} Incresaing Packet queues ${NC}"

sudo sysctl  -w  net.core.netdev_max_backlog=5000

echo "${green} Increasing TCP TIME_WAIT tuning ${NC}"

sudo sysctl -w net.ipv4.tcp_max_tw_buckets=7200000
