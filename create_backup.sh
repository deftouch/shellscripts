#!/bin/bash

export PATH=/bin:/usr/bin:/usr/local/bin
TODAY=`date +"%d%b%Y"`

DB_BACKUP_PATH='/home/venkat/deftouch/backup'
MYSQL_HOST='localhost'
MYSQL_PORT='3306'
MYSQL_USER='root'
MYSQL_PASSWORD='deftouchAdmin@7'
DATABASE_NAME='mydb'
BACKUP_RETAIN_DAYS=1   ## Number of days to keep local backup copy

#################################################################

echo "Backup started for database - ${DATABASE_NAME}"
mkdir -p ${DB_BACKUP_PATH}/${TODAY}
#mysqldump -h ${MYSQL_HOST} \
#                 -P ${MYSQL_PORT} \
#                 -u ${MYSQL_USER} \
#                 -p${MYSQL_PASSWORD} \
#                 ${DATABASE_NAME} | gzip > ${DB_BACKUP_PATH}/${TODAY}/${DATABASE_NAME}-${TODAY}.sql.gz
mysqldump -u ${MYSQL_USER} \
               -p${MYSQL_PASSWORD} \
              --all-databases --master-data > ${DB_BACKUP_PATH}/${TODAY}/${DATABASE_NAME}-${TODAY}.db


if [ $? -eq 0 ]; then
  echo "Database backup successfully completed"
else
  echo "Error found during backup"
fi


##### Remove backups older than {BACKUP_RETAIN_DAYS} days  #####

DBDELDATE=`date +"%d%b%Y" --date="${BACKUP_RETAIN_DAYS} days ago"`

if [ ! -z ${DB_BACKUP_PATH} ]; then
      cd ${DB_BACKUP_PATH}
      if [ ! -z ${DBDELDATE} ] && [ -d ${DBDELDATE} ]; then
            rm -rf ${DBDELDATE}
      fi
fi

### End of script ####

